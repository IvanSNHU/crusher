﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

//this script sets a few global settings and manages the game music
public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    [SerializeField] int frameRate = 60;
    AudioSource music;

    private void Awake()
    {
        if (instance != this && instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        Application.targetFrameRate = frameRate;
        music = GetComponent<AudioSource>();
        music.volume = PlayerPrefs.GetFloat("musicVolume", 1.0f); ;
        music.mute = bool.Parse(PlayerPrefs.GetString("musicToggle", bool.FalseString)); ;
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {

    }

    //this function is called when the audio settings are changed
    public void updateAudioSettings()
    {
        music.volume = PlayerPrefs.GetFloat("musicVolume", 1.0f); ;
        music.mute = bool.Parse(PlayerPrefs.GetString("musicToggle", bool.FalseString)); ;
    }

    //format the time string to look good
    public string timeString(float hours, float minutes, float seconds)
    {
        string returnMe = "";

        if (hours > 0)
        {
            returnMe = seconds.ToString("00");
            returnMe = returnMe.Insert(0, minutes.ToString("00") + ':');
            returnMe = returnMe.Insert(0, hours.ToString("00") + ':');
        }
        else if (minutes > 0)
        {
            returnMe = seconds.ToString("00");
            returnMe = returnMe.Insert(0, minutes.ToString("00") + ':');
        }
        else
        {
            returnMe = seconds.ToString("00");
        }
        returnMe = returnMe.Insert(0, "Time: ");

        return returnMe;
    }
}
