﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.Playables;
using UnityEngine.UI;

//this script controls the UI during the main game phase
public class CanvasManager : MonoBehaviour
{
    public static CanvasManager instance;
    [SerializeField] Text pointsField, timeField, shotsField;
    [SerializeField] GameObject noMatchPanel;
    int points, shots;
    float seconds, minutes, hours;
    float timeSinceLastUpdate = 0;

    private void Awake()
    {
        if (instance != this && instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        points = 0;
        pointsField.text = "Points: " + points.ToString();
        shots = 0;
        shotsField.text = "Shots: " + shots.ToString();

        seconds = 0.0f;
        minutes = 0.0f;
        hours = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        //this set makes it so that we only update the time field when it needs to be updated (once a second)
        timeSinceLastUpdate += Time.deltaTime;
        seconds += Time.deltaTime;
        if (timeSinceLastUpdate >= 1.0f)
        {
            timeSinceLastUpdate -= 1.0f;
            if (seconds >= 60.0f)
            {
                seconds -= 60.0f;
                minutes++;
            }
            if (minutes >= 60.0f)
            {
                minutes -= 60.0f;
                hours++;
            }

            timeField.text = timeString();
        }
    }

    //format the time string to look good
    string timeString()
    {
        return GameManager.instance.timeString(hours, minutes, seconds);
    }

    //the player got points, update the field
    public void addPoints(byte newPoints)
    {
        points += (int)(newPoints + (newPoints - 1));
        pointsField.text = "Points: " + points.ToString();
    }

    //the player tried to pop something, add an attempt
    public void addShot()
    {
        shots++;
        shotsField.text = "Shots: " + shots.ToString();
    }

    //old method of writing the time string
    string floatToTime(float input)
    {
        string returnMe;

        byte hours = (byte)(input / 3600.0f);
        byte minutes = (byte)((input - (hours * 3600.0f)) / 60.0f);
        byte seconds = (byte)(input - (hours * 3600.0f) - (minutes * 60.0f));

        if (hours > 0)
        {
            returnMe = seconds.ToString("00");
            returnMe = returnMe.Insert(0, minutes.ToString("00") + ':');
            returnMe = returnMe.Insert(0, hours.ToString("00") + ':');
        }
        else if (minutes > 0)
        {
            returnMe = seconds.ToString("00");
            returnMe = returnMe.Insert(0, minutes.ToString("00") + ':');
        }
        else
        {
            returnMe = seconds.ToString("00");
        }
        returnMe = returnMe.Insert(0, "Time: ");

        return returnMe;
    }

    //save high score
    private void OnDestroy()
    {
        float tempTime = (hours * 3600) + (minutes * 60) + seconds;
        float savedTime =
            (PlayerPrefs.GetFloat("hours", 0) * 3600) +
            (PlayerPrefs.GetFloat("minutes", 0) * 60) +
            PlayerPrefs.GetFloat("seconds", 0);
        if (tempTime > savedTime)
        {
            PlayerPrefs.SetFloat("seconds", seconds);
            PlayerPrefs.SetFloat("minutes", minutes);
            PlayerPrefs.SetFloat("hours", hours);
        }
        if (PlayerPrefs.GetInt("points", 0) < points)
            PlayerPrefs.SetInt("points", points);
        if (PlayerPrefs.GetInt("shots", 0) < shots)
            PlayerPrefs.SetInt("shots", shots);

        PlayerPrefs.Save();
    }

    //we found no matches. let the user know
    public void noMatches()
    {
        noMatchPanel.SetActive(true);
    }
}
