﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.Tilemaps;

//this script controls the popping of tiles
//the generating of tiles
//and tile like things
public class PopManager : MonoBehaviour
{
    [SerializeField] AudioSource popSound;
    [SerializeField] GameObject patsy;
    [SerializeField] int gridx, gridy;
    [SerializeField] byte minMatch = 0;
    [SerializeField] float maxClickRate = 0.5f;
    public static PopManager instance;
    public Sprite[] shapesSprites;
    [SerializeField] Vector2 gridStart;
    public float tileSpacing;
    public Dictionary<Vector2, Poppable> patsies = new Dictionary<Vector2, Poppable>();
    float lastClick = 1.0f;

    private void Awake()
    {
        if (instance != this && instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        //load how-many-tiles-must-match setting. increment by one because it is stored at one less.
        minMatch = (byte)PlayerPrefs.GetInt("minMatchVal", 0);
        minMatch++;
        //generate the starting set
        for (int x = 0; x < gridx; x++)
        {
            for (int y = 0; y < gridy; y++)
            {
                GameObject temp = Instantiate(patsy);
                Vector2 tempPosition = new Vector2(gridStart.x + (tileSpacing * x), gridStart.y + (tileSpacing * y));
                temp.transform.position = tempPosition;
                temp.transform.SetParent(gameObject.transform);
                Poppable tempPoppable = temp.GetComponent<Poppable>();
                tempPoppable.myShape = (byte)UnityEngine.Random.Range(0, shapesSprites.Length);
                tempPoppable.mySpot = tempPosition;
                patsies.Add(tempPosition, tempPoppable);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        //there is a max speed for clicks
        lastClick += Time.deltaTime;
    }

    //first thing called when a tile is clicked
    public void Ouch(Vector2 input)
    {
        if (lastClick >= maxClickRate)
        {
            CanvasManager.instance.addShot();
            crawler(input);
        }
    }

    //"pathfinding system" that figures out how big the match is
    void crawler(Vector2 inputPosition)
    {
        bool minMet = false;
        Poppable inputPoppable = patsies[inputPosition];
        List<Poppable> discovered = new List<Poppable>();
        List<Poppable> needsScan = new List<Poppable>();
        List<Poppable> scanned = new List<Poppable>();
        Dictionary<float, byte> toInstantiate = new Dictionary<float, byte>();

        //fetch every object that matches this column value
        List<Poppable> fetchColumn(float x)
        {
            List<Poppable> returnMe = new List<Poppable>();
            foreach (var target in patsies)
            {
                if (target.Key.x == x)
                {
                    returnMe.Add(target.Value);
                }
            }

            return returnMe;
        }
        //see if we've seen this before. add new stuff to 'discovered'
        //add new stuff of the right shape to needsScan
        void Seen(Vector2 newbie)
        {
            if (patsies.ContainsKey(newbie))
            {
                Poppable temp = patsies[newbie];
                if (!discovered.Contains(temp))
                {
                    discovered.Add(temp);
                    if (temp.myShape == inputPoppable.myShape)
                    {
                        needsScan.Add(temp);
                    }
                }
            }
        }
        //check all the cardinals of this spot, mark this one as scanned
        void Scan(Poppable newbie)
        {
            needsScan.Remove(newbie);
            scanned.Add(newbie);
            Seen(newbie.mySpot + (Vector2.up * tileSpacing));
            Seen(newbie.mySpot + (Vector2.down * tileSpacing));
            Seen(newbie.mySpot + (Vector2.left * tileSpacing));
            Seen(newbie.mySpot + (Vector2.right * tileSpacing));
        }
        //safely remove a single match, update the placement values of its column, queue up a new object creation
        void Remove(Poppable weWhoAreAboutToDie)
        {
            float columnFocus = weWhoAreAboutToDie.mySpot.x;
            patsies.Remove(weWhoAreAboutToDie.mySpot);
            var tempList = fetchColumn(weWhoAreAboutToDie.mySpot.x);
            foreach (var target in tempList)
            {
                patsies.Remove(target.mySpot);
            }
            foreach (var target in tempList)
            {
                if (target.mySpot.y > weWhoAreAboutToDie.mySpot.y)
                {
                    target.mySpot.y -= tileSpacing;
                    patsies.Add(target.mySpot, target);
                }
                else
                {
                    patsies.Add(target.mySpot, target);
                }
            }
            Destroy(weWhoAreAboutToDie.gameObject);
            if (toInstantiate.ContainsKey(columnFocus))
            {
                toInstantiate[columnFocus]++;
            }
            else
            {
                toInstantiate.Add(columnFocus, 1);
            }
        }
        //create a new object at this position
        void addToPosition(Vector2 newPosition)
        {
            GameObject temp = Instantiate(patsy);
            temp.transform.position = new Vector2(newPosition.x, newPosition.y + (gridy * tileSpacing));
            temp.transform.SetParent(gameObject.transform);
            temp.GetComponent<Rigidbody2D>().velocity = Vector2.down * 6.0f;

            Poppable tempPoppable = temp.GetComponent<Poppable>();
            tempPoppable.myShape = (byte)UnityEngine.Random.Range(0, shapesSprites.Length);
            tempPoppable.mySpot = newPosition;
            patsies.Add(newPosition, tempPoppable);
        }
        //count how many shapes match in this combo
        byte comboCount(byte shape, List<Poppable> inputList)
        {
            byte returnMe = 0;
            foreach (var target in inputList)
            {
                if (target.myShape == shape)
                {
                    returnMe++;
                }
            }
            return returnMe;
        }

        //the actual discovery loop
        Seen(inputPosition);
        for (int i = 0; i <= gridx * gridy; i++)
        {
            if (needsScan.Count > 0)
            {
                Scan(needsScan[0]);
            }
            else
            {
                byte targetCount = comboCount(inputPoppable.myShape, discovered);
                if (targetCount >= minMatch)
                {
                    minMet = true;
                    CanvasManager.instance.addPoints(targetCount);
                }
                break;
            }
            if (i == gridx * gridy)
            {
                Debug.LogError("Discovery loop overflowed!");
            }
        }

        //is the match big enough?
        if (!minMet)
            return;

        //everything that needs to happen on a good match
        popSound.Play();
        lastClick = 0.0f;

        //get rid of the stuff that is popped
        foreach (var target in discovered)
        {
            if (target.myShape == inputPoppable.myShape)
            {
                Remove(target);
            }
        }

        //process filling in the empty spots in columns
        foreach (var target in toInstantiate)
        {
            for (int i = target.Value; i > 0; i--)
            {
                Vector2 newPosition = new Vector2();
                newPosition.x = target.Key;
                //gridStart.y + (tileSpacing * y));
                newPosition.y = gridStart.y + (tileSpacing * (gridy - i));
                addToPosition(newPosition);
            }
        }

        //check for remaining matches
        if (minMatch > 1)
            remainingMatches();
    }

    //return true if this position is a match
    bool miniCrawler(Vector2 inputPosition)
    {
        Poppable inputPoppable = patsies[inputPosition];
        List<Poppable> discovered = new List<Poppable>();
        List<Poppable> needsScan = new List<Poppable>();
        List<Poppable> scanned = new List<Poppable>();
        //Dictionary<float, byte> toInstantiate = new Dictionary<float, byte>();

        //fetch every object that matches this column value
        //see if we've seen this before. add new stuff to 'discovered'
        //add new stuff of the right shape to needsScan
        void Seen(Vector2 newbie)
        {
            if (patsies.ContainsKey(newbie))
            {
                Poppable temp = patsies[newbie];
                if (!discovered.Contains(temp))
                {
                    discovered.Add(temp);
                    if (temp.myShape == inputPoppable.myShape)
                    {
                        needsScan.Add(temp);
                    }
                }
            }
        }
        //check all the cardinals of this spot, mark this one as scanned
        void Scan(Poppable newbie)
        {
            needsScan.Remove(newbie);
            scanned.Add(newbie);
            Seen(newbie.mySpot + (Vector2.up * tileSpacing));
            Seen(newbie.mySpot + (Vector2.down * tileSpacing));
            Seen(newbie.mySpot + (Vector2.left * tileSpacing));
            Seen(newbie.mySpot + (Vector2.right * tileSpacing));
        }
        //count how many shapes match in this combo
        byte comboCount(byte shape, List<Poppable> inputList)
        {
            byte returnMe = 0;
            foreach (var target in inputList)
            {
                if (target.myShape == shape)
                {
                    returnMe++;
                }
            }
            return returnMe;
        }

        //the actual discovery loop
        Seen(inputPosition);
        for (int i = 0; i <= gridx * gridy; i++)
        {
            if (needsScan.Count > 0)
            {
                Scan(needsScan[0]);
            }
            else
            {
                byte targetCount = comboCount(inputPoppable.myShape, discovered);
                if (targetCount >= minMatch)
                {
                    return true;
                }
                break;
            }
            if (i == gridx * gridy)
            {
                Debug.LogError("Discovery loop overflowed!");
            }
        }

        return false;
    }

    void remainingMatches()
    {
        //search for matches
        foreach (var target in patsies)
        {
            if (miniCrawler(target.Key))
            {
                //if we found a match, do nothing
                return;
            }
        }

        //if we found no matches, let the user know
        CanvasManager.instance.noMatches();
    }
}
