﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

//this script is for the settings page
public class SettingsManager : MonoBehaviour
{
    [SerializeField] Slider musicSlider, sfxSlider;
    [SerializeField] Toggle musicMute, sfxMute;
    [SerializeField] Dropdown minMatchDropdown;
    Single musicVolume, sfxVolume;
    Boolean musicToggle, sfxToggle;
    Int32 minMatchVal;

    // Start is called before the first frame update
    void Start()
    {
        //load the settings or load the defaults
        musicVolume = PlayerPrefs.GetFloat("musicVolume", 1.0f);
        sfxVolume = PlayerPrefs.GetFloat("sfxVolume", 0.5f);
        musicToggle = bool.Parse(PlayerPrefs.GetString("musicToggle", bool.FalseString));
        sfxToggle = bool.Parse(PlayerPrefs.GetString("sfxToggle", bool.FalseString));
        minMatchVal = PlayerPrefs.GetInt("minMatchVal", 0);

        //make the UI match what was loaded
        musicSlider.value = musicVolume;
        sfxSlider.value = sfxVolume;
        musicMute.isOn = musicToggle;
        sfxMute.isOn = sfxToggle;
        minMatchDropdown.value = minMatchVal;
    }

    // Update is called once per frame
    void Update()
    {

    }

    //these are callbacks from the UI
    public void updateMusic(Single value)
    {
        musicVolume = value;
    }
    public void toggleMusic(Boolean value)
    {
        musicToggle = value;
    }
    public void updateSFX(Single value)
    {
        sfxVolume = value;
    }
    public void toggleSFX(Boolean value)
    {
        sfxToggle = value;
    }
    public void minMatchChange(Int32 value)
    {
        minMatchVal = value;
    }

    //called when we leave the settings page
    private void OnDestroy()
    {
        //set the settings to match UI changes
        PlayerPrefs.SetFloat("musicVolume", musicVolume);
        PlayerPrefs.SetFloat("sfxVolume", sfxVolume);
        PlayerPrefs.SetString("musicToggle", musicToggle.ToString());
        PlayerPrefs.SetString("sfxToggle", sfxToggle.ToString());
        PlayerPrefs.SetInt("minMatchVal", minMatchVal);

        //save the settings, tell the music to update
        PlayerPrefs.Save();
        GameManager.instance.updateAudioSettings();
    }
}
