﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//this object needs to play its sound when an impact breaks the magnitude break.
public class PlaySoundOnImpact : MonoBehaviour
{
    [SerializeField] AudioSource relevantAudio;
    [SerializeField] float magnitudeGate;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.relativeVelocity.sqrMagnitude >= Mathf.Pow(magnitudeGate, 2))
        {
            relevantAudio.Play();
        }
    }
}
