﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//this script gets added to anything that has sound effects.
//set this volume to the sfx volume
public class LoadSFXVolume : MonoBehaviour
{
    AudioSource mySource;

    // Start is called before the first frame update
    void Start()
    {
        if (TryGetComponent<AudioSource>(out mySource))
        {
            mySource.volume = PlayerPrefs.GetFloat("sfxVolume", 0.5f);
            mySource.mute = bool.Parse(PlayerPrefs.GetString("sfxToggle", bool.FalseString));
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
