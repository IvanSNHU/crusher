﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//manages the high score display on the home screen
public class HighScoreManager : MonoBehaviour
{
    [SerializeField] Text time, points, shots;

    // Start is called before the first frame update
    void Start()
    {
        //load/display high scores. shows zeroes if it can't find any.
        time.text = GameManager.instance.timeString(
            PlayerPrefs.GetFloat("hours", 0),
            PlayerPrefs.GetFloat("minutes", 0),
            PlayerPrefs.GetFloat("seconds", 0));

        points.text = "Points: " + PlayerPrefs.GetInt("points", 0);

        shots.text = "Shots: " + PlayerPrefs.GetInt("shots", 0);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
