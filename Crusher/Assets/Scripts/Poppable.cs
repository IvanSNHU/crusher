﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;

//this cript goes on tiles that can be popped
public class Poppable : MonoBehaviour, IPointerClickHandler
{
    SpriteRenderer mySR;
    public byte myShape;
    public Vector2 mySpot;

    // Start is called before the first frame update
    void Start()
    {
        //change sprite to match the assigned shape
        mySR = GetComponent<SpriteRenderer>();
        mySR.sprite = PopManager.instance.shapesSprites[myShape];
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        //process a click. happenes in the manager
        PopManager.instance.Ouch(mySpot);
    }
}
