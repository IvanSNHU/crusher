﻿#include "pch-c.h"


#include "codegen/il2cpp-codegen-metadata.h"





extern void CanvasManager_Awake_m2A29860466F79A9BAEDCAC00C94288214684E735 (void);
extern void CanvasManager_Start_m4461F6B90F6BB2930D09B0CF482AC3C617E5A360 (void);
extern void CanvasManager_Update_m9384F080EB11902407B89AC4819099F984019AD6 (void);
extern void CanvasManager_timeString_m42C6D23194848905E78456510076EADD4D04A7AB (void);
extern void CanvasManager_addPoints_m486986213B05E45154F4916098AACF1983EA34CC (void);
extern void CanvasManager_addShot_m9143F4FA115A422276B751F384D6E5C9561A884D (void);
extern void CanvasManager_floatToTime_mF96B00A8ED7D5ED73FC6774E26A5428BCF2E7588 (void);
extern void CanvasManager_OnDestroy_m1A269277BB8453A5FEFC177EAED34F5211BDFE80 (void);
extern void CanvasManager_noMatches_m9FA8823E9ED80D3D69154473728499ED76116DA6 (void);
extern void CanvasManager__ctor_m8D01C316B4C83FEB1E2FF919AB552C47AC9FB33F (void);
extern void GameManager_Awake_m4B6E8E2AF58C95C9A2A0C4637A34AE0892CB637F (void);
extern void GameManager_Start_m87A71D65F3171A58DBDDBFB03832ADA65643D0E2 (void);
extern void GameManager_Update_m7F29D8E933B8D21D2E67507979C0F12ACF87BB41 (void);
extern void GameManager_updateAudioSettings_m1055FB92A7A6977DCFBC698A0B1CF9DF60109821 (void);
extern void GameManager_timeString_m8362749DEB8DE044338AB3F3FB18F3AC5985A2FF (void);
extern void GameManager__ctor_mF453CED520617BFB65C52405A964E06CF17DB368 (void);
extern void HighScoreManager_Start_m47F53BC4376EAD00C4A7461219FE5063A4441C9D (void);
extern void HighScoreManager_Update_mDE4C034244EA9D48239581B5886BA5804EE568CE (void);
extern void HighScoreManager__ctor_m3E3C2D89CF5E838C760F9A54B3DF6B5A3F5438D7 (void);
extern void LoadSFXVolume_Start_mAF0617A00389BF66CE202691C13C1B24F17E4DDB (void);
extern void LoadSFXVolume_Update_m0328B89C05E41867DFC34766F1267A33C66B81A2 (void);
extern void LoadSFXVolume__ctor_mA3B7BDC3396ABBE486FB5D808D5EFA2280C9286C (void);
extern void PlaySoundOnImpact_Start_mA5098889571BC0E7314A232D269243D6F39E58A5 (void);
extern void PlaySoundOnImpact_Update_m8692AB14E4ACB65A8BF6911929F601ADEE00F4E4 (void);
extern void PlaySoundOnImpact_OnCollisionEnter2D_m67F2CEA59E34AC90335B151CFE91A3767D7F0173 (void);
extern void PlaySoundOnImpact__ctor_mCCAFB70B7087CE0D11E8DCB85471CE46D3DA0259 (void);
extern void PopManager_Awake_mF67937CC764DAED824885F87230937313C80D652 (void);
extern void PopManager_Start_m74D40020E13B1ED61E7857592D221D4798F683E9 (void);
extern void PopManager_Update_m42230C813FD4E9E4CEF892010A1DF7F22C1AF1CA (void);
extern void PopManager_Ouch_mE044C6A54591FC7051D559A6E850807BF4EBB14A (void);
extern void PopManager_crawler_m64D79208194902E6F93527D0584311EA1A2CC887 (void);
extern void PopManager_miniCrawler_m4B582A2A5D1579B45A37DCA75F8E88A480D0D70D (void);
extern void PopManager_remainingMatches_mF5E4BEC3445C6156B73DCA6C634E037FFA0B9216 (void);
extern void PopManager__ctor_mFA5C4046F11BFA660977387B6C569E2BC051F51E (void);
extern void PopManager_U3CcrawlerU3Eg__fetchColumnU7C16_0_m491E0463B379643BEA5D7A61DE4C5F8837A76047 (void);
extern void PopManager_U3CcrawlerU3Eg__SeenU7C16_1_m9C52D924E7CD8FE20DCB9E33E933E95B8B7D8B77 (void);
extern void PopManager_U3CcrawlerU3Eg__ScanU7C16_2_mDCAEF67FB67421B88B98F24124EAC513208E2825 (void);
extern void PopManager_U3CcrawlerU3Eg__RemoveU7C16_3_mAE87A3DDC664D665DE2B562BAA982E54B05A3299 (void);
extern void PopManager_U3CcrawlerU3Eg__addToPositionU7C16_4_m84FCE0106C9B1D7D317DB386F61E099569DB1150 (void);
extern void PopManager_U3CcrawlerU3Eg__comboCountU7C16_5_mEA996576200FA59D6B8C68AB1FA7DF126E7A5B7A (void);
extern void PopManager_U3CminiCrawlerU3Eg__SeenU7C17_0_m1C03938A525BAAFDA4668F29E338DB2F0713E842 (void);
extern void PopManager_U3CminiCrawlerU3Eg__ScanU7C17_1_mF64AAE2ECBE4F3D4991A962EC502BEB64E6ABD5B (void);
extern void PopManager_U3CminiCrawlerU3Eg__comboCountU7C17_2_m1CC3601052761377E4DF86D4D39037FB18A576A5 (void);
extern void Poppable_Start_m216C0E0029C0FF5133C2B74AE3523A900F50F948 (void);
extern void Poppable_OnPointerClick_m32B762C6CA66110AF05581D514A4E4997F8AE105 (void);
extern void Poppable__ctor_m45A03F8B2EA1690CBD4D098D4F9DCCB386719E44 (void);
extern void SceneLoader_Start_mB9AA9E8ADCE59F893E3EF8E891ED5E1F3AB80DA0 (void);
extern void SceneLoader_Update_m1F381D68B1B1F69A0F9A5876813BB7D437E3A713 (void);
extern void SceneLoader_LoadScene_mFE87E2AA20B80CBECD7C02C3A6EF68661736A3F1 (void);
extern void SceneLoader__ctor_m2248766DF38AF07562AD31501C7275B8DF1B7D29 (void);
extern void SettingsManager_Start_mCB2940703E340E9B826D2A92F76DAB09F8B7B487 (void);
extern void SettingsManager_Update_mA208CCFBCEE9C1092EC521E2F61A1C7993AAFE21 (void);
extern void SettingsManager_updateMusic_m2EA3361496B73107337EB6745576475A546C68DC (void);
extern void SettingsManager_toggleMusic_mDF54A61E1AF5F43796AA49669EF53196EFE2D824 (void);
extern void SettingsManager_updateSFX_m4FA47C16DA3AA4BA79D91B5C84434FE4BAF14B93 (void);
extern void SettingsManager_toggleSFX_m3618EB4746D959FA93D6AF84A08859231A14DDF8 (void);
extern void SettingsManager_minMatchChange_mF90213FED560B7CF3459F03CBD35C670483B0A19 (void);
extern void SettingsManager_OnDestroy_m68449A69B8CA6F746041AD5C6C54B9DD50078E0B (void);
extern void SettingsManager__ctor_m3F050F09698FD9788C28F8F171CA891F45BC4D10 (void);
extern void UnitySourceGeneratedAssemblyMonoScriptTypes_v1_Get_mBEB95BEB954BB63E9710BBC7AD5E78C4CB0A0033 (void);
extern void UnitySourceGeneratedAssemblyMonoScriptTypes_v1__ctor_mE70FB23ACC1EA12ABC948AA22C2E78B2D0AA39B1 (void);
static Il2CppMethodPointer s_methodPointers[61] = 
{
	CanvasManager_Awake_m2A29860466F79A9BAEDCAC00C94288214684E735,
	CanvasManager_Start_m4461F6B90F6BB2930D09B0CF482AC3C617E5A360,
	CanvasManager_Update_m9384F080EB11902407B89AC4819099F984019AD6,
	CanvasManager_timeString_m42C6D23194848905E78456510076EADD4D04A7AB,
	CanvasManager_addPoints_m486986213B05E45154F4916098AACF1983EA34CC,
	CanvasManager_addShot_m9143F4FA115A422276B751F384D6E5C9561A884D,
	CanvasManager_floatToTime_mF96B00A8ED7D5ED73FC6774E26A5428BCF2E7588,
	CanvasManager_OnDestroy_m1A269277BB8453A5FEFC177EAED34F5211BDFE80,
	CanvasManager_noMatches_m9FA8823E9ED80D3D69154473728499ED76116DA6,
	CanvasManager__ctor_m8D01C316B4C83FEB1E2FF919AB552C47AC9FB33F,
	GameManager_Awake_m4B6E8E2AF58C95C9A2A0C4637A34AE0892CB637F,
	GameManager_Start_m87A71D65F3171A58DBDDBFB03832ADA65643D0E2,
	GameManager_Update_m7F29D8E933B8D21D2E67507979C0F12ACF87BB41,
	GameManager_updateAudioSettings_m1055FB92A7A6977DCFBC698A0B1CF9DF60109821,
	GameManager_timeString_m8362749DEB8DE044338AB3F3FB18F3AC5985A2FF,
	GameManager__ctor_mF453CED520617BFB65C52405A964E06CF17DB368,
	HighScoreManager_Start_m47F53BC4376EAD00C4A7461219FE5063A4441C9D,
	HighScoreManager_Update_mDE4C034244EA9D48239581B5886BA5804EE568CE,
	HighScoreManager__ctor_m3E3C2D89CF5E838C760F9A54B3DF6B5A3F5438D7,
	LoadSFXVolume_Start_mAF0617A00389BF66CE202691C13C1B24F17E4DDB,
	LoadSFXVolume_Update_m0328B89C05E41867DFC34766F1267A33C66B81A2,
	LoadSFXVolume__ctor_mA3B7BDC3396ABBE486FB5D808D5EFA2280C9286C,
	PlaySoundOnImpact_Start_mA5098889571BC0E7314A232D269243D6F39E58A5,
	PlaySoundOnImpact_Update_m8692AB14E4ACB65A8BF6911929F601ADEE00F4E4,
	PlaySoundOnImpact_OnCollisionEnter2D_m67F2CEA59E34AC90335B151CFE91A3767D7F0173,
	PlaySoundOnImpact__ctor_mCCAFB70B7087CE0D11E8DCB85471CE46D3DA0259,
	PopManager_Awake_mF67937CC764DAED824885F87230937313C80D652,
	PopManager_Start_m74D40020E13B1ED61E7857592D221D4798F683E9,
	PopManager_Update_m42230C813FD4E9E4CEF892010A1DF7F22C1AF1CA,
	PopManager_Ouch_mE044C6A54591FC7051D559A6E850807BF4EBB14A,
	PopManager_crawler_m64D79208194902E6F93527D0584311EA1A2CC887,
	PopManager_miniCrawler_m4B582A2A5D1579B45A37DCA75F8E88A480D0D70D,
	PopManager_remainingMatches_mF5E4BEC3445C6156B73DCA6C634E037FFA0B9216,
	PopManager__ctor_mFA5C4046F11BFA660977387B6C569E2BC051F51E,
	PopManager_U3CcrawlerU3Eg__fetchColumnU7C16_0_m491E0463B379643BEA5D7A61DE4C5F8837A76047,
	PopManager_U3CcrawlerU3Eg__SeenU7C16_1_m9C52D924E7CD8FE20DCB9E33E933E95B8B7D8B77,
	PopManager_U3CcrawlerU3Eg__ScanU7C16_2_mDCAEF67FB67421B88B98F24124EAC513208E2825,
	PopManager_U3CcrawlerU3Eg__RemoveU7C16_3_mAE87A3DDC664D665DE2B562BAA982E54B05A3299,
	PopManager_U3CcrawlerU3Eg__addToPositionU7C16_4_m84FCE0106C9B1D7D317DB386F61E099569DB1150,
	PopManager_U3CcrawlerU3Eg__comboCountU7C16_5_mEA996576200FA59D6B8C68AB1FA7DF126E7A5B7A,
	PopManager_U3CminiCrawlerU3Eg__SeenU7C17_0_m1C03938A525BAAFDA4668F29E338DB2F0713E842,
	PopManager_U3CminiCrawlerU3Eg__ScanU7C17_1_mF64AAE2ECBE4F3D4991A962EC502BEB64E6ABD5B,
	PopManager_U3CminiCrawlerU3Eg__comboCountU7C17_2_m1CC3601052761377E4DF86D4D39037FB18A576A5,
	Poppable_Start_m216C0E0029C0FF5133C2B74AE3523A900F50F948,
	Poppable_OnPointerClick_m32B762C6CA66110AF05581D514A4E4997F8AE105,
	Poppable__ctor_m45A03F8B2EA1690CBD4D098D4F9DCCB386719E44,
	SceneLoader_Start_mB9AA9E8ADCE59F893E3EF8E891ED5E1F3AB80DA0,
	SceneLoader_Update_m1F381D68B1B1F69A0F9A5876813BB7D437E3A713,
	SceneLoader_LoadScene_mFE87E2AA20B80CBECD7C02C3A6EF68661736A3F1,
	SceneLoader__ctor_m2248766DF38AF07562AD31501C7275B8DF1B7D29,
	SettingsManager_Start_mCB2940703E340E9B826D2A92F76DAB09F8B7B487,
	SettingsManager_Update_mA208CCFBCEE9C1092EC521E2F61A1C7993AAFE21,
	SettingsManager_updateMusic_m2EA3361496B73107337EB6745576475A546C68DC,
	SettingsManager_toggleMusic_mDF54A61E1AF5F43796AA49669EF53196EFE2D824,
	SettingsManager_updateSFX_m4FA47C16DA3AA4BA79D91B5C84434FE4BAF14B93,
	SettingsManager_toggleSFX_m3618EB4746D959FA93D6AF84A08859231A14DDF8,
	SettingsManager_minMatchChange_mF90213FED560B7CF3459F03CBD35C670483B0A19,
	SettingsManager_OnDestroy_m68449A69B8CA6F746041AD5C6C54B9DD50078E0B,
	SettingsManager__ctor_m3F050F09698FD9788C28F8F171CA891F45BC4D10,
	UnitySourceGeneratedAssemblyMonoScriptTypes_v1_Get_mBEB95BEB954BB63E9710BBC7AD5E78C4CB0A0033,
	UnitySourceGeneratedAssemblyMonoScriptTypes_v1__ctor_mE70FB23ACC1EA12ABC948AA22C2E78B2D0AA39B1,
};
static const int32_t s_InvokerIndices[61] = 
{
	4765,
	4765,
	4765,
	4665,
	3798,
	4765,
	3389,
	4765,
	4765,
	4765,
	4765,
	4765,
	4765,
	4765,
	858,
	4765,
	4765,
	4765,
	4765,
	4765,
	4765,
	4765,
	4765,
	4765,
	3876,
	4765,
	4765,
	4765,
	4765,
	3948,
	3948,
	2830,
	4765,
	4765,
	1560,
	2101,
	2037,
	2037,
	2101,
	6119,
	2101,
	2037,
	6119,
	4765,
	3876,
	4765,
	4765,
	4765,
	3876,
	4765,
	4765,
	4765,
	3910,
	3798,
	3910,
	3798,
	3847,
	4765,
	4765,
	7233,
	4765,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	61,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
